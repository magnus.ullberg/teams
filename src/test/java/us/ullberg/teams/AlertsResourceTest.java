package us.ullberg.teams;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class AlertsResourceTest {

    @Test
    public void testAlertsEndpoint() {
        given()
        .when()
            .post("/alerts")
        .then()
            .statusCode(200)
            .body(is("Invalid JSON"));

        given().body("{\"receiver\":\"webhook\",\"status\":\"firing\",\"alerts\":[{\"status\":\"firing\",\"labels\":{\"alertname\":\"HighRequestPerSecond\",\"instance\":\"192.168.69.20:8080\",\"job\":\"teamsbot\",\"monitor\":\"example\",\"severity\":\"page\"},\"annotations\":{\"summary\":\"High request latency\"},\"startsAt\":\"2019-10-04T19:16:16.848466326-04:00\",\"endsAt\":\"0001-01-01T00:00:00Z\",\"generatorURL\":\"http://node1:9090/graph?g0.expr=rate%28application_us_ullberg_teams_alertsReceived_get_total%5B1m%5D%29+%3E+0.1&g0.tab=1\"}],\"groupLabels\":{\"job\":\"teamsbot\"},\"commonLabels\":{\"alertname\":\"HighRequestPerSecond\",\"instance\":\"192.168.69.20:8080\",\"job\":\"teamsbot\",\"monitor\":\"example\",\"severity\":\"page\"},\"commonAnnotations\":{\"summary\":\"High request latency\"},\"externalURL\":\"http://node1:9093\",\"version\":\"4\",\"groupKey\":\"{}:{job=\\\"teamsbot\\\"}\"}")
        .when()
            .post("/alerts")
        .then()
            .statusCode(200)
            .body(is("OK"));
    }
}