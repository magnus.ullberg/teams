package us.ullberg.teams;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Timed;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/alerts")
@Counted(name = "alertsReceived", description = "How many alerts have been received.")
@Timed(name = "alertsTimer", description = "A measure of how long it takes to process a alert.", unit = MetricUnits.MILLISECONDS)
public class AlertsResource {
    private static final Logger LOGGER = Logger.getLogger(AlertsResource.class.getName());

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String post(String Body) {
        JsonObject message;
        try {
            message = new JsonObject(Body);
            LOGGER.log(Level.INFO, "JSON: {0}", message.toString());
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, ex.toString(), ex);
            
            return "Invalid JSON";
        }

        String receiver = message.getString("receiver");
        String externalURL = message.getString("externalURL");
        JsonArray alerts = message.getJsonArray("alerts");

        for (Object o : alerts) {
            JsonObject jo = (JsonObject) o;

            LOGGER.log(Level.INFO, "============");
            LOGGER.log(Level.INFO, "Alert Name: {0}", jo.getJsonObject("labels").getString("alertname"));
            LOGGER.log(Level.INFO, "Receiver: {0}", receiver);
            LOGGER.log(Level.INFO, "Status: {0}", jo.getString("status"));
            LOGGER.log(Level.INFO, "ExternalURL: {0}", externalURL);
            LOGGER.log(Level.INFO, "GeneratorURL: {0}", jo.getString("generatorURL"));

            LOGGER.log(Level.INFO, "Labels:");
            logProperties(jo, "labels");

            LOGGER.log(Level.INFO, "Annotations:");
            logProperties(jo, "annotations");
        }
        return "OK";
    }

    private void logProperties(JsonObject jo, String keyname) {
        for (String key : jo.getJsonObject(keyname).fieldNames()) {
            LOGGER.log(Level.INFO, " key[{0}] value[{1}]",
                    new String[] { key, jo.getJsonObject(keyname).getString(key) });
        }
    }
}
